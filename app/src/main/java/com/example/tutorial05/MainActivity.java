package com.example.tutorial05;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    List<String> countryList = Arrays.asList("Sri lanka","Japan","India");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridLayout gridLayout = findViewById(R.id.flag_layout);
        View layoutInflater = getLayoutInflater().inflate(R.layout.country, gridLayout);

        //fkfkfkyfkfl
        //ftfkk
    }

    private void createCountriesLayout(String country, GridLayout gridLayout){
        String name = country.toLowerCase().replaceAll(" ", "");
        View layoutInflater = getLayoutInflater().inflate(R.layout.country, gridLayout);
        ImageButton flagImg = layoutInflater.findViewById(R.id.flag_img);
        TextView countryName = layoutInflater.findViewById(R.id.country_name);
        int image = getResources().getIdentifier(name, "drawable", getPackageName());
        flagImg.setImageResource(image);
        countryName.setText(country);

        flagImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
            }
        });

        gridLayout.addView(layoutInflater);

    }

    private void showContryInfo(String originalName, String convertedCountry){

        int file = getResources().getIdentifier(convertedCountry, "raw", getPackageName());
        String countryDetails = new Scanner(getResources().openRawResource(file)).nextLine();

        String countryNameWithAnthem = convertedCountry.concat("_anthem");
        int countryAnthemId = getResources().getIdentifier(countryNameWithAnthem, "raw", getPackageName());
        final MediaPlayer mediaPlayer = MediaPlayer.create(this, countryAnthemId);
        mediaPlayer.start();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(originalName);
        builder.setMessage(originalName);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mediaPlayer.stop();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }

}